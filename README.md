# element-desktop-git

[AUR - Element Desktop](https://aur.archlinux.org/packages/element-desktop-git)
[AUR - Element Web](https://aur.archlinux.org/packages/element-web-git)

[Upstream - Element Desktop](https://github.com/vector-im/element-desktop)
[Upstream - Element Web](https://github.com/vector-im/element-web)
